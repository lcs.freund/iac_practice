#!/bin/bash

set -e

echo " Cloning Repository "
git clone https://github.com/Artemmkin/raddit.git

echo " install dependent gems"
cd ./raddit
sudo bundle install

echo "-- starting app --" 
sudo systemctl start raddit
sudo systemctl enable raddit


